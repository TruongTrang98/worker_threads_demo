
// Cách bình thường: Generate 1tr name với 2.56 minute
/*
const fs = require("fs-extra");
const {
  getRandomIndex
} = require("./utils")
const firstName = require("./data/first-name.json");
const middleName = require("./data/middle-name.json");
const lastName = require("./data/last-name.json");
const limit = 1000000;
const outputFile = `${__dirname}/output/data.txt`;
(async () => {
  console.time("TIME")
  for (let i = 0; i < limit; i++) {
    const data = [firstName, middleName, lastName]
      .map(getRandomIndex)
      .concat("\n")
      .join(" ");
    await fs.appendFile(outputFile, data);
  }
  console.timeEnd("TIME")
})();
*/


// Cách 2: Dùng worker: Generate 1tr name với 1.45 minute
const { Worker } = require('worker_threads');
const logUpdate = require("log-update");
const limit = 1000000;
const threads = 10;
const namesPerThread = limit / threads;
const outputFile = `${__dirname}/output/data_with_worker.txt`;
let names = [...Array(threads)].fill(0);

for (let i = 0; i < threads; i++) {
  const port = new Worker(require.resolve("./worker.js"), {
    workerData: {
      namesPerThread,
      outputFile
    }
  });
  port.on("message", data => { });
  port.on("error", (e) => console.log(e));
  port.on("exit", (code) => console.log(`Exit code: ${code}`));
}

function handleMessage(_, index) {
  names[index]++;
  logUpdate(names.map((status, i) => `Thread ${i}: ${status}`).join("\n"));
}
