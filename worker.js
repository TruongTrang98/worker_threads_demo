const { getRandomIndex } = require("./utils");
const {
  parentPort,
  workerData
} = require("worker_threads")

const fs = require("fs-extra");
const firstName = require("./data/first-name.json");
const middleName = require("./data/middle-name.json");
const lastName = require("./data/last-name.json");

const {
  namesPerThread,
  outputFile
} = workerData;


!async function () {
  console.time("TIME");
  for (let i = 0; i < namesPerThread; i++) {
    const data = [firstName, middleName, lastName]
      .map(getRandomIndex)
      .concat("\n")
      .join(" ");
    await fs.appendFile(outputFile, data);
    parentPort.postMessage(data);
  }
  console.timeEnd("TIME")
}()